# Test multilingual data in json

## Install dependencies and run
```bash
# Open Terminal and run
pip3 install requirements.txt
python3 srv.py

# Open another Terminal and run
python3 cli.py
```
